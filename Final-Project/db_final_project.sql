-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Jan 2022 pada 06.02
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_final_project`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `developers`
--

CREATE TABLE `developers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext DEFAULT NULL,
  `website` longtext DEFAULT NULL,
  `twitter` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `developer_members`
--

CREATE TABLE `developer_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `developer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `games`
--

CREATE TABLE `games` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext DEFAULT NULL,
  `price` longtext DEFAULT NULL,
  `release_date` datetime(3) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `game_developers`
--

CREATE TABLE `game_developers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `game_id` bigint(20) UNSIGNED DEFAULT NULL,
  `developer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `game_genres`
--

CREATE TABLE `game_genres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `game_id` bigint(20) UNSIGNED DEFAULT NULL,
  `genre_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `game_publishers`
--

CREATE TABLE `game_publishers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `game_id` bigint(20) UNSIGNED DEFAULT NULL,
  `publisher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `genres`
--

CREATE TABLE `genres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `my_games`
--

CREATE TABLE `my_games` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `game_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `publishers`
--

CREATE TABLE `publishers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext DEFAULT NULL,
  `website` longtext DEFAULT NULL,
  `twitter` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `publisher_members`
--

CREATE TABLE `publisher_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `publisher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `game_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` longtext DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_comments`
--

CREATE TABLE `review_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `review_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comment` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_likes`
--

CREATE TABLE `review_likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `review_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext DEFAULT NULL,
  `role_write_permission` tinyint(1) DEFAULT NULL,
  `user_write_permission` tinyint(1) DEFAULT NULL,
  `game_write_permission` tinyint(1) DEFAULT NULL,
  `review_write_permission` tinyint(1) DEFAULT NULL,
  `my_game_write_permission` tinyint(1) DEFAULT NULL,
  `wishlist_write_permission` tinyint(1) DEFAULT NULL,
  `developer_write_permission` tinyint(1) DEFAULT NULL,
  `publisher_write_permission` tinyint(1) DEFAULT NULL,
  `developer_member_write_permission` tinyint(1) DEFAULT NULL,
  `publisher_member_write_permission` tinyint(1) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `role_write_permission`, `user_write_permission`, `game_write_permission`, `review_write_permission`, `my_game_write_permission`, `wishlist_write_permission`, `developer_write_permission`, `publisher_write_permission`, `developer_member_write_permission`, `publisher_member_write_permission`, `created_at`, `updated_at`) VALUES
(1, 'Master', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2022-01-28 10:27:08.179', '2022-01-28 10:27:08.179'),
(2, 'Admin', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2022-01-28 10:27:08.179', '2022-01-28 10:27:08.179'),
(3, 'Developer', 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, '2022-01-28 10:27:08.179', '2022-01-28 10:27:08.179'),
(4, 'Publisher', 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, '2022-01-28 10:27:08.179', '2022-01-28 10:27:08.179'),
(5, 'Member', 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, '2022-01-28 10:27:08.179', '2022-01-28 10:27:08.179'),
(6, 'Master', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2022-01-31 11:57:01.199', '2022-01-31 11:57:01.199'),
(7, 'Admin', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2022-01-31 11:57:01.199', '2022-01-31 11:57:01.199'),
(8, 'Developer', 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, '2022-01-31 11:57:01.199', '2022-01-31 11:57:01.199'),
(9, 'Publisher', 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, '2022-01-31 11:57:01.199', '2022-01-31 11:57:01.199'),
(10, 'Member', 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, '2022-01-31 11:57:01.199', '2022-01-31 11:57:01.199');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `leader` tinyint(1) DEFAULT NULL,
  `fullname` longtext DEFAULT NULL,
  `nickname` longtext DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `username` varchar(191) NOT NULL,
  `password` longtext DEFAULT NULL,
  `age` bigint(20) DEFAULT NULL,
  `gender` longtext DEFAULT NULL,
  `date_of_birth` datetime(3) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `leader`, `fullname`, `nickname`, `email`, `username`, `password`, `age`, `gender`, `date_of_birth`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Master', 'Master', 'master@master.master', 'master', '$2a$10$bUZCm/WgSYxT8lU2MRmZhuu0s7FuEzItBMDYSrgpP393HwlY5MPni', 100, 'Master', '2022-01-28 10:27:08.156', '2022-01-28 10:27:08.581', '2022-01-28 10:27:08.581');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wishlists`
--

CREATE TABLE `wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `game_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `developers`
--
ALTER TABLE `developers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `developer_members`
--
ALTER TABLE `developer_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_developer_members` (`user_id`),
  ADD KEY `fk_developers_developer_members` (`developer_id`);

--
-- Indeks untuk tabel `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `game_developers`
--
ALTER TABLE `game_developers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_games_game_developers` (`game_id`),
  ADD KEY `fk_developers_game_developers` (`developer_id`);

--
-- Indeks untuk tabel `game_genres`
--
ALTER TABLE `game_genres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_genres_game_genres` (`genre_id`),
  ADD KEY `fk_games_game_genres` (`game_id`);

--
-- Indeks untuk tabel `game_publishers`
--
ALTER TABLE `game_publishers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_publishers_game_publishers` (`publisher_id`),
  ADD KEY `fk_games_game_publishers` (`game_id`);

--
-- Indeks untuk tabel `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `my_games`
--
ALTER TABLE `my_games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_games_my_games` (`game_id`),
  ADD KEY `fk_users_my_games` (`user_id`);

--
-- Indeks untuk tabel `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `publisher_members`
--
ALTER TABLE `publisher_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_publishers_publisher_members` (`publisher_id`),
  ADD KEY `fk_users_publisher_members` (`user_id`);

--
-- Indeks untuk tabel `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_reviews` (`user_id`),
  ADD KEY `fk_games_reviews` (`game_id`);

--
-- Indeks untuk tabel `review_comments`
--
ALTER TABLE `review_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reviews_review_comments` (`review_id`);

--
-- Indeks untuk tabel `review_likes`
--
ALTER TABLE `review_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reviews_review_likes` (`review_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `fk_roles_users` (`role_id`);

--
-- Indeks untuk tabel `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_wishlists` (`user_id`),
  ADD KEY `fk_games_wishlists` (`game_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `developers`
--
ALTER TABLE `developers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `developer_members`
--
ALTER TABLE `developer_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `games`
--
ALTER TABLE `games`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `game_developers`
--
ALTER TABLE `game_developers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `game_genres`
--
ALTER TABLE `game_genres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `game_publishers`
--
ALTER TABLE `game_publishers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `genres`
--
ALTER TABLE `genres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `my_games`
--
ALTER TABLE `my_games`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `publisher_members`
--
ALTER TABLE `publisher_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `review_comments`
--
ALTER TABLE `review_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `review_likes`
--
ALTER TABLE `review_likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `developer_members`
--
ALTER TABLE `developer_members`
  ADD CONSTRAINT `fk_developers_developer_members` FOREIGN KEY (`developer_id`) REFERENCES `developers` (`id`),
  ADD CONSTRAINT `fk_users_developer_members` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `game_developers`
--
ALTER TABLE `game_developers`
  ADD CONSTRAINT `fk_developers_game_developers` FOREIGN KEY (`developer_id`) REFERENCES `developers` (`id`),
  ADD CONSTRAINT `fk_games_game_developers` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`);

--
-- Ketidakleluasaan untuk tabel `game_genres`
--
ALTER TABLE `game_genres`
  ADD CONSTRAINT `fk_games_game_genres` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `fk_genres_game_genres` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`);

--
-- Ketidakleluasaan untuk tabel `game_publishers`
--
ALTER TABLE `game_publishers`
  ADD CONSTRAINT `fk_games_game_publishers` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `fk_publishers_game_publishers` FOREIGN KEY (`publisher_id`) REFERENCES `publishers` (`id`);

--
-- Ketidakleluasaan untuk tabel `my_games`
--
ALTER TABLE `my_games`
  ADD CONSTRAINT `fk_games_my_games` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `fk_users_my_games` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `publisher_members`
--
ALTER TABLE `publisher_members`
  ADD CONSTRAINT `fk_publishers_publisher_members` FOREIGN KEY (`publisher_id`) REFERENCES `publishers` (`id`),
  ADD CONSTRAINT `fk_users_publisher_members` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `fk_games_reviews` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `fk_users_reviews` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `review_comments`
--
ALTER TABLE `review_comments`
  ADD CONSTRAINT `fk_reviews_review_comments` FOREIGN KEY (`review_id`) REFERENCES `reviews` (`id`);

--
-- Ketidakleluasaan untuk tabel `review_likes`
--
ALTER TABLE `review_likes`
  ADD CONSTRAINT `fk_reviews_review_likes` FOREIGN KEY (`review_id`) REFERENCES `reviews` (`id`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_roles_users` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `wishlists`
--
ALTER TABLE `wishlists`
  ADD CONSTRAINT `fk_games_wishlists` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `fk_users_wishlists` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
