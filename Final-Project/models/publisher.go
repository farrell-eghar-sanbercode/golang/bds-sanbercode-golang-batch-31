package models

import "time"

type (
	Publisher struct {
		ID               uint              `json:"id" gorm:"primary_key"`
		Name             string            `json:"name"`
		Website          string            `json:"website"`
		Twitter          string            `json:"twitter"`
		CreatedAt        time.Time         `json:"created_at"`
		UpdatedAt        time.Time         `json:"updated_at"`
		GamePublishers   []GamePublisher   `json:"-"`
		PublisherMembers []PublisherMember `json:"-"`
	}
)
