package models

import "time"

type (
	DeveloperMember struct {
		ID          uint      `json:"id" gorm:"primary_key"`
		UserID      uint      `json:"userID"`
		DeveloperID uint      `json:"developerID"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		User        User      `json:"-"`
		Developer   Developer `json:"-"`
	}
)
