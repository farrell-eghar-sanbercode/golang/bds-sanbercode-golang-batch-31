package models

import "time"

type (
	Game struct {
		ID             uint            `json:"id" gorm:"primary_key"`
		Name           string          `json:"name"`
		Price          string          `json:"price"`
		ReleaseDate    time.Time       `json:"release_date"`
		CreatedAt      time.Time       `json:"created_at"`
		UpdatedAt      time.Time       `json:"updated_at"`
		MyGames        []MyGame        `json:"-"`
		Wishlists      []Wishlist      `json:"-"`
		Reviews        []Review        `json:"-"`
		GameGenres     []GameGenre     `json:"-"`
		GameDevelopers []GameDeveloper `json:"-"`
		GamePublishers []GamePublisher `json:"-"`
	}
)
