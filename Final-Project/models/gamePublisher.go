package models

import "time"

type (
	GamePublisher struct {
		ID          uint      `json:"id" gorm:"primary_key"`
		GameID      uint      `json:"gameID"`
		PublisherID uint      `json:"publisherID"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		Game        Game      `json:"-"`
		Publisher   Publisher `json:"-"`
	}
)
