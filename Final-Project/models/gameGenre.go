package models

import "time"

type (
	GameGenre struct {
		ID        uint      `json:"id" gorm:"primary_key"`
		GameID    uint      `json:"gameID"`
		GenreID   uint      `json:"genreID"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
		Game      Game      `json:"-"`
		Genre     Genre     `json:"-"`
	}
)
