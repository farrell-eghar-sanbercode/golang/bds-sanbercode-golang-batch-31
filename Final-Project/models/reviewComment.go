package models

import "time"

type (
	ReviewComment struct {
		ID        uint      `json:"id" gorm:"primary_key"`
		UserID    uint      `json:"userID"`
		ReviewID  uint      `json:"reviewID"`
		Comment   string    `json:"comment"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
		User      User      `json:"-"`
		Review    Review    `json:"-"`
	}
)
