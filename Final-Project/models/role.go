package models

import "time"

type (
	Role struct {
		ID                             uint      `json:"id" gorm:"primary_key"`
		Name                           string    `json:"name"`
		RoleWritePermission            bool      `json:"role_write_permission"`
		UserWritePermission            bool      `json:"user_write_permission"`
		GameWritePermission            bool      `json:"game_write_permission"`
		ReviewWritePermission          bool      `json:"review_write_permission"`
		MyGameWritePermission          bool      `json:"my_game_write_permission"`
		WishlistWritePermission        bool      `json:"wishlist_write_permission"`
		DeveloperWritePermission       bool      `json:"developer_write_permission"`
		PublisherWritePermission       bool      `json:"publisher_write_permission"`
		DeveloperMemberWritePermission bool      `json:"developer_member_write_permission"`
		PublisherMemberWritePermission bool      `json:"publisher_member_write_permission"`
		CreatedAt                      time.Time `json:"created_at"`
		UpdatedAt                      time.Time `json:"updated_at"`
		Users                          []User    `json:"-"`
	}
)
