package models

import "time"

type (
	PublisherMember struct {
		ID          uint      `json:"id" gorm:"primary_key"`
		UserID      uint      `json:"userID"`
		PublisherID uint      `json:"publisherID"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		User        User      `json:"-"`
		Publisher   Publisher `json:"-"`
	}
)
