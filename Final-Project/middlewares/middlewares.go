package middlewares

import (
	"Final-Project/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
)

func JWTAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		c.Next()
	}
}

func RolePermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := RolePermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func UserPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := UserPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func GamePermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := GamePermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func ReviewPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := ReviewPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func MyGamePermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := MyGamePermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func WishlistPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := WishlistPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func DeveloperPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := DeveloperPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func PublisherPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := PublisherPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func DevMemberPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := DevMemberPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}

func PubMemberPermissionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}

		status, res := PubMemberPermissionValidation(c)
		if status != 200 {
			c.JSON(status, res)
			c.Abort()
			return
		}

		c.Next()
	}
}
