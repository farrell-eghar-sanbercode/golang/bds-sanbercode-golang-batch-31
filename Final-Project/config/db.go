package config

import (
	"Final-Project/models"
	"fmt"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectDatabase() *gorm.DB {
	username := "root"
	password := ""
	host := "tcp(127.0.0.1:3306)"
	database := "db_final_project"

	dsn := fmt.Sprintf(
		"%v:%v@%v/%v?charset=utf8mb4&parseTime=True&loc=Local",
		username,
		password,
		host,
		database,
	)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err.Error())
	}

	db.AutoMigrate(
		&models.Developer{},
		&models.DeveloperMember{},
		&models.Game{},
		&models.GameDeveloper{},
		&models.GameGenre{},
		&models.GamePublisher{},
		&models.Genre{},
		&models.MyGame{},
		&models.Publisher{},
		&models.PublisherMember{},
		&models.Review{},
		&models.ReviewComment{},
		&models.ReviewLike{},
		&models.Role{},
		&models.User{},
		&models.Wishlist{},
	)

	InitialData(db)

	return db
}

func InitialData(db *gorm.DB) {
	var user models.User
	if err := db.First(&user, 1).Error; err != nil {
		var roles []models.Role
		master := models.Role{
			Name:                           "Master",
			RoleWritePermission:            true,
			UserWritePermission:            true,
			GameWritePermission:            true,
			ReviewWritePermission:          true,
			MyGameWritePermission:          true,
			WishlistWritePermission:        true,
			DeveloperWritePermission:       true,
			PublisherWritePermission:       true,
			DeveloperMemberWritePermission: true,
			PublisherMemberWritePermission: true,
		}
		admin := models.Role{
			Name:                           "Admin",
			RoleWritePermission:            false,
			UserWritePermission:            true,
			GameWritePermission:            true,
			ReviewWritePermission:          true,
			MyGameWritePermission:          true,
			WishlistWritePermission:        true,
			DeveloperWritePermission:       true,
			PublisherWritePermission:       true,
			DeveloperMemberWritePermission: true,
			PublisherMemberWritePermission: true,
		}
		developer := models.Role{
			Name:                           "Developer",
			RoleWritePermission:            false,
			UserWritePermission:            false,
			GameWritePermission:            true,
			ReviewWritePermission:          true,
			MyGameWritePermission:          true,
			WishlistWritePermission:        true,
			DeveloperWritePermission:       true,
			PublisherWritePermission:       false,
			DeveloperMemberWritePermission: true,
			PublisherMemberWritePermission: false,
		}
		publisher := models.Role{
			Name:                           "Publisher",
			RoleWritePermission:            false,
			UserWritePermission:            false,
			GameWritePermission:            true,
			ReviewWritePermission:          true,
			MyGameWritePermission:          true,
			WishlistWritePermission:        true,
			DeveloperWritePermission:       false,
			PublisherWritePermission:       true,
			DeveloperMemberWritePermission: false,
			PublisherMemberWritePermission: true,
		}
		member := models.Role{
			Name:                           "Member",
			RoleWritePermission:            false,
			UserWritePermission:            false,
			GameWritePermission:            false,
			ReviewWritePermission:          true,
			MyGameWritePermission:          true,
			WishlistWritePermission:        true,
			DeveloperWritePermission:       false,
			PublisherWritePermission:       false,
			DeveloperMemberWritePermission: false,
			PublisherMemberWritePermission: false,
		}
		roles = append(roles, master, admin, developer, publisher, member)

		user := models.User{
			Fullname:    "Master",
			Nickname:    "Master",
			Email:       "master@master.master",
			Username:    "master",
			Password:    "CallMeMaster",
			Age:         100,
			Gender:      "Master",
			DateOfBirth: time.Now(),
			RoleID:      1,
			Leader:      true,
		}
		db.Create(&roles)

		_, err := user.SaveUser(db)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Printf("%s\n\nROLES INITIALIZATION ARE RUNNING WELL!\n", string("\033[36m"))
		fmt.Printf("MASTER ACCOUNT:\nUSERNAME: master\nPASSWORD: CallMeMaster\n\n%s\n", string("\033[0m"))
	} else {
		fmt.Printf("%s\n\nROLES HAS BEEN INITIALIZED!\n", string("\033[36m"))
		fmt.Printf("DEFAULT MASTER ACCOUNT:\nUSERNAME: master\nPASSWORD: CallMeMaster\n\n%s\n", string("\033[0m"))
	}
}
