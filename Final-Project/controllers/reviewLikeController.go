package controllers

import (
	"Final-Project/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// CreateReviewLike godoc
// @Summary Create New Review Like.
// @Description Creating a new Review Like.
// @Tags ReviewLike
// @Param reviewID path string true "Review id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.ReviewLike
// @Router /review-like/{reviewID} [post]
func CreateReviewLike(c *gin.Context) {
	conv, _ := strconv.Atoi(c.Param("reviewID"))

	like := models.ReviewLike{
		UserID:   models.LoggedInUser.ID,
		ReviewID: uint(conv),
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&like)

	c.JSON(http.StatusCreated, gin.H{"data": like})
}

// DeleteReviewLike godoc
// @Summary Delete one Review Like.
// @Description Delete a Review Like by id.
// @Tags ReviewLike
// @Produce json
// @Param reviewID path string true "Review id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /review-like/{reviewID} [delete]
func DeleteReviewLike(c *gin.Context) {
	var like models.ReviewLike
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&like, c.Param("reviewID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&like)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
