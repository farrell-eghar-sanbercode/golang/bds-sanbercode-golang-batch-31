package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	roleInput struct {
		Name string `json:"name"`
	}
)

// GetAllRoles godoc
// @Summary Get all Roles.
// @Description Get a list of Roles.
// @Tags Role
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.Role
// @Router /roles [get]
func GetAllRoles(c *gin.Context) {
	var roles []models.Role
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&roles)

	c.JSON(http.StatusOK, gin.H{"data": roles})
}

// CreateRole godoc
// @Summary Create New Role.
// @Description Creating a new Role.
// @Tags Role
// @Produce json
// @Param Body body roleInput true "the body to create a new Role"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Role
// @Router /role [post]
func CreateRole(c *gin.Context) {
	var input roleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	role := models.Role{
		Name: input.Name,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&role)

	c.JSON(http.StatusCreated, gin.H{"data": role})
}

// GetRoleByID godoc
// @Summary Get Role.
// @Description Get a Role by id.
// @Tags Role
// @Produce json
// @Param roleID path string true "Role id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Role
// @Router /role/{roleID} [get]
func GetRoleByID(c *gin.Context) {
	var role models.Role
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&role, c.Param("roleID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": role})
}

// GetUsersByRoleID godoc
// @Summary Get User.
// @Description Get all Users by RoleID.
// @Tags Role
// @Produce json
// @Param roleID path string true "Role id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.User
// @Router /role/{roleID}/users [get]
func GetUsersByRoleID(c *gin.Context) {
	var users []models.User
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Find(&users, "role_id = ?", c.Param("roleID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": users})
}

// UpdateRole godoc
// @Summary Update Role.
// @Description Update Role by id.
// @Tags Role
// @Produce json
// @Param roleID path string true "Role id"
// @Param Body body roleInput true "the body to update Role"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Role
// @Router /role/{roleID} [put]
func UpdateRole(c *gin.Context) {
	var input roleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var role models.Role
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&role, c.Param("roleID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedRole models.Role
	updatedRole.Name = input.Name
	updatedRole.UpdatedAt = time.Now()

	db.Model(&role).Updates(updatedRole)
	c.JSON(http.StatusOK, gin.H{"data": role})
}

// DeleteRole godoc
// @Summary Delete one Role.
// @Description Delete a Role by id.
// @Tags Role
// @Produce json
// @Param roleID path string true "Role id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /role/{roleID} [delete]
func DeleteRole(c *gin.Context) {
	var role models.Role
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Find(&role, c.Param("roleID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&role)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
