package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	gameGenreInput struct {
		GameID  uint `json:"game_id"`
		GenreID uint `json:"genre_id"`
	}
)

func GetAllGameGenres(c *gin.Context) {
	var gameGenres []models.GameGenre
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&gameGenres)

	c.JSON(http.StatusOK, gin.H{"data": gameGenres})
}

func CreateGameGenre(c *gin.Context) {
	var input gameGenreInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	gameGenre := models.GameGenre{
		GameID:  input.GameID,
		GenreID: input.GenreID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&gameGenre)

	c.JSON(http.StatusCreated, gin.H{"data": gameGenre})
}

func GetGameGenreByID(c *gin.Context) {
	var gameGenre models.GameGenre
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gameGenre, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": gameGenre})
}

func UpdateGameGenre(c *gin.Context) {
	var input gameGenreInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var gameGenre models.GameGenre
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gameGenre, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedGameGenre models.GameGenre
	updatedGameGenre.GameID = input.GameID
	updatedGameGenre.GenreID = input.GenreID
	updatedGameGenre.UpdatedAt = time.Now()

	db.Model(&gameGenre).Updates(updatedGameGenre)
	c.JSON(http.StatusOK, gin.H{"data": gameGenre})
}

func DeleteGameGenre(c *gin.Context) {
	var gameGenre models.GameGenre
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gameGenre, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&gameGenre)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
