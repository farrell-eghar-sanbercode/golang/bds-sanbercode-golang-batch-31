package controllers

import (
	"Final-Project/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	loginInput struct {
		Username string `json:"username" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	registerInput struct {
		Email    string `json:"email" binding:"required"`
		Username string `json:"username" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	changePasswordInput struct {
		Password     string `json:"password" binding:"required"`
		Confirmation string `json:"confirmation" binding:"required"`
	}
)

// LoginUser godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body loginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
	var input loginInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	token, err := models.LoginCheck(input.Username, input.Password, db)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Username or Password is incorrect!"})
		return
	}

	user := map[string]string{
		"username": input.Username,
		"password": input.Password,
	}
	c.JSON(http.StatusOK, gin.H{"message": "You are logged in!", "user": user, "token": token})
}

// Register godoc
// @Summary Register a user.
// @Description registering a user from public access.
// @Tags Auth
// @Param Body body registerInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
	var input registerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{
		Email:    input.Email,
		Username: input.Username,
		Password: input.Password,
	}
	db := c.MustGet("db").(*gorm.DB)
	_, err := u.SaveUser(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"email":    u.Email,
		"username": u.Username,
	}
	c.JSON(http.StatusCreated, gin.H{"message": "You are registered!", "user": user})
}

// ChangePassword godoc
// @Summary Change a user password.
// @Description changing a user password.
// @Tags Auth
// @Produce json
// @Param Body body changePasswordInput true "the body to change a user password"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]interface{}
// @Router /my/profile/change-password [post]
func ChangePassowrd(c *gin.Context) {
	var input changePasswordInput
	user := models.LoggedInUser

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if input.Password != input.Confirmation {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password and Confirmation should not be different!"})
		return
	}

	user.Password = input.Password
	db := c.MustGet("db").(*gorm.DB)
	_, err := user.ChangePassowrd(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := map[string]string{
		"email":    user.Email,
		"username": user.Username,
	}
	c.JSON(http.StatusCreated, gin.H{"message": "You are registered!", "user": u})
}
