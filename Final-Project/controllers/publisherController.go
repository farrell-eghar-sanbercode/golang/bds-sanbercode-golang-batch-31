package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	publisherInput struct {
		Name    string `json:"name"`
		Website string `json:"website"`
		Twitter string `json:"twitter"`
	}
)

// GetAllPublishers godoc
// @Summary Get all Publishers Company.
// @Description Get a list of Publishers Company.
// @Tags Publisher
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.Publisher
// @Router /publishers [get]
func GetAllPublishers(c *gin.Context) {
	var publishers []models.Publisher
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&publishers)

	c.JSON(http.StatusOK, gin.H{"data": publishers})
}

// CreatePublisher godoc
// @Summary Create New Publisher.
// @Description Creating a new Publisher Company.
// @Tags Publisher
// @Param Body body publisherInput true "the body to create a new Publisher"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Publisher
// @Router /publisher [post]
func CreatePublisher(c *gin.Context) {
	var input publisherInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	publisher := models.Publisher{
		Name:    input.Name,
		Website: input.Website,
		Twitter: input.Twitter,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&publisher)

	c.JSON(http.StatusCreated, gin.H{"data": publisher})
}

// GetPublisherByID godoc
// @Summary Get Publisher Company.
// @Description Get a Publisher by id.
// @Tags Publisher
// @Produce json
// @Param pubID path string true "Publisher id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Publisher
// @Router /publisher/{pubID} [get]
func GetPublisherByID(c *gin.Context) {
	var publisher models.Publisher
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&publisher, c.Param("pubID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": publisher})
}

// GetUsersByPublisherID godoc
// @Summary Get Users.
// @Description Get all Users by PublisherID.
// @Tags Publisher
// @Produce json
// @Param pubID path string true "Publisher id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.User
// @Router /publisher/{pubID}/users [get]
func GetUsersByPublisherID(c *gin.Context) {
	var pubMembers []models.PublisherMember
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("publisher_id = ?", c.Param("pubID")).Preload("User").Find(&pubMembers).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": pubMembers})
}

// UpdatePublisher godoc
// @Summary Update Publisher Company.
// @Description Update Publisher Company by id.
// @Tags Publisher
// @Produce json
// @Param pubID path string true "Publisher id"
// @Param Body body publisherInput true "the body to update Publisher Company"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Publisher
// @Router /publisher/{pubID} [put]
func UpdatePublisher(c *gin.Context) {
	var input publisherInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var publisher models.Publisher
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&publisher, c.Param("pubID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedPublisher models.Publisher
	updatedPublisher.Name = input.Name
	updatedPublisher.Website = input.Website
	updatedPublisher.Twitter = input.Twitter
	updatedPublisher.UpdatedAt = time.Now()

	db.Model(&publisher).Updates(updatedPublisher)
	c.JSON(http.StatusOK, gin.H{"data": publisher})
}

// DeletePublisher godoc
// @Summary Delete one Publisher Company.
// @Description Delete a Publisher Company by id.
// @Tags Publisher
// @Produce json
// @Param pubID path string true "Publisher id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /publisher/{pubID} [delete]
func DeletePublisher(c *gin.Context) {
	var publisher models.Publisher
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&publisher, c.Param("pubID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&publisher)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
