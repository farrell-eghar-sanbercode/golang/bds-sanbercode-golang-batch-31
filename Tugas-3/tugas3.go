package main

import (
	"fmt"
	"strconv"
)

func main() {
	// Soal 1
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"
	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	panjangPersegiPanjangInt, _ := (strconv.ParseInt(panjangPersegiPanjang, 10, 8))
	lebarPersegiPanjangInt, _ := strconv.ParseInt(lebarPersegiPanjang, 10, 4)
	alasSegitigaInt, _ := strconv.ParseInt(alasSegitiga, 10, 4)
	tinggiSegitigaInt, _ := strconv.ParseInt(tinggiSegitiga, 10, 4)

	var luasPersegiPanjang int
	var kelilingPersegiPanjang int
	var luasSegitiga int
	luasPersegiPanjang = int(panjangPersegiPanjangInt) * int(lebarPersegiPanjangInt)
	kelilingPersegiPanjang = 2 * (int(panjangPersegiPanjangInt) + int(lebarPersegiPanjangInt))
	luasSegitiga = (int(alasSegitigaInt) * int(tinggiSegitigaInt)) / 2

	fmt.Println("Luas Persegi Panjang : " + strconv.Itoa(luasPersegiPanjang))
	fmt.Println("Keliling Persegi Panjang : " + strconv.Itoa(kelilingPersegiPanjang))
	fmt.Println("Luas Segitiga : " + strconv.Itoa(luasSegitiga) + "\n")

	// Soal 2
	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilai := nilaiJohn; nilai >= 80 && nilai <= 100 {
		fmt.Println("Indeks nilai John adalah A")
	} else if nilai >= 70 && nilai < 80 {
		fmt.Println("Indeks nilai John adalah B")
	} else if nilai >= 60 && nilai < 70 {
		fmt.Println("Indeks nilai John adalah C")
	} else if nilai >= 50 && nilai < 60 {
		fmt.Println("Indeks nilai John adalah D")
	} else if nilai >= 0 && nilai < 50 {
		fmt.Println("Indeks nilai John adalah E")
	} else {
		fmt.Println("Nilai John tidak valid!")
	}

	if nilai := nilaiDoe; nilai >= 80 && nilai <= 100 {
		fmt.Println("Indeks nilai Doe adalah A")
	} else if nilai >= 70 && nilai < 80 {
		fmt.Println("Indeks nilai Doe adalah B")
	} else if nilai >= 60 && nilai < 70 {
		fmt.Println("Indeks nilai Doe adalah C")
	} else if nilai >= 50 && nilai < 60 {
		fmt.Println("Indeks nilai Doe adalah D")
	} else if nilai >= 0 && nilai < 50 {
		fmt.Println("Indeks nilai Doe adalah E")
	} else {
		fmt.Println("Nilai Doe tidak valid!")
	}

	// Soal 3
	var tanggal = 04
	var bulan = 9
	var tahun = 2001

	switch bulan {
	case 1:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Januari " + strconv.Itoa(tahun))
	case 2:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Februari " + strconv.Itoa(tahun))
	case 3:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Maret " + strconv.Itoa(tahun))
	case 4:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " April " + strconv.Itoa(tahun))
	case 5:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Mei " + strconv.Itoa(tahun))
	case 6:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Juni " + strconv.Itoa(tahun))
	case 7:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Juli " + strconv.Itoa(tahun))
	case 8:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Agustus " + strconv.Itoa(tahun))
	case 9:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " September " + strconv.Itoa(tahun))
	case 10:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Oktober " + strconv.Itoa(tahun))
	case 11:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " November " + strconv.Itoa(tahun))
	case 12:
		fmt.Println("\nAku lahir pada " + strconv.Itoa(tanggal) + " Desember " + strconv.Itoa(tahun))
	default:
		fmt.Println("\nBulan yang anda masukkan tidak valid!")
	}

	// Soal 4
	if tahun >= 1994 && tahun <= 1964 {
		fmt.Println(`Sehingga orang memanggilku 'Baby Boomer'`)
	} else if tahun >= 1965 && tahun <= 1979 {
		fmt.Println(`Sehingga orang memanggilku 'Generasi X'`)
	} else if tahun >= 1980 && tahun <= 1994 {
		fmt.Println(`Sehingga orang memanggilku 'Generasi Millenials'`)
	} else if tahun >= 1995 && tahun <= 2015 {
		fmt.Println(`Sehingga orang memanggilku 'Generasi Z'`)
	} else {
		fmt.Println("Tahun yang anda masukkan tidak valid!")
	}
}
